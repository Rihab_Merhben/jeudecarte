package com.example.demo.service;

import com.example.demo.model.Card;
import com.example.demo.model.Color;
import com.example.demo.model.Hand;
import com.example.demo.model.Value;
import com.example.demo.repository.JeuRepository;

import java.util.*;
import java.util.stream.Collectors;

public class JeuServiceImpl implements JeuService{

    private JeuRepository jeuRepository;

    private List<Color> colors = Arrays.asList(Color.values());
    private List<Value> values = Arrays.asList(Value.values());
    private static final Random RANDOM = new Random();
    private static final List<Color> COLORS =
            Collections.unmodifiableList(Arrays.asList(Color.values()));
    private static final int SIZE_COLORS = COLORS.size();
    private static final List<Value> VALUES =
            Collections.unmodifiableList(Arrays.asList(Value.values()));
    private static final int SIZE_VALUES = VALUES.size();
    public JeuServiceImpl(JeuRepository jeuRepository) {
        this.jeuRepository = jeuRepository;
    }

    public List<Color> sortRandomColor() {
        return sortRandom(colors);
    }

    public List sortRandomValue() {
        return sortRandom(values);
    }

    private List sortRandom(List list) {
        Collections.shuffle(list);
        return list;
    }

    public Hand tenCardInHand() {
        List<Card> cards = new ArrayList<>();
        Hand hand = new Hand(cards);
        for(int i=0; i<10; i++) {
            Card card = new Card();
            card.setColor(COLORS.get(RANDOM.nextInt(SIZE_COLORS)));
            card.setValue(VALUES.get(RANDOM.nextInt(SIZE_VALUES)));
            cards.add(card);
        }
        return hand;
    }

    public Hand sortHand(List<Card> cards, List<Color> colors, List<Value> values) {
        List<Card> sortedCards = new ArrayList<>();
        Hand sortedHand = new Hand(sortedCards);
        Collections.sort(cards, Comparator.comparing(item ->
                colors.indexOf(item.getColor())));

        for(Color color: colors) {
            List<Card> newCards = cards.stream().filter(s -> s.getColor().equals(color))
                    .collect(Collectors.toList());;
            Collections.sort(newCards, Comparator.comparing(item ->
                    values.indexOf(item.getValue())));
            sortedCards.addAll(newCards);
        }
        sortedHand.setCards(sortedCards);
        return sortedHand;
    }
}
