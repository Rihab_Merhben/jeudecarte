package com.example.demo.service;

import com.example.demo.model.Card;
import com.example.demo.model.Color;
import com.example.demo.model.Hand;
import com.example.demo.model.Value;

import java.util.List;

public interface JeuService {

    public List sortRandomColor();

    public List sortRandomValue();

    public Hand tenCardInHand();

    public Hand sortHand(List<Card> cards, List<Color> colors, List<Value> values);
}
