package com.example.demo.model;

import lombok.Builder;
import java.util.List;

@Builder
public class Hand {
    List<Card> cards;

    public Hand(List<Card> cards) {
        this.cards = cards;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }
}
