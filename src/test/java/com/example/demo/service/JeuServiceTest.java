package com.example.demo.service;

import com.example.demo.model.Card;
import com.example.demo.model.Color;
import com.example.demo.model.Hand;
import com.example.demo.model.Value;
import com.example.demo.repository.JeuRepository;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.mock;

public class JeuServiceTest {

    @Test
    public void shouldInitializeGame() {
        Hand hand = Hand.builder().cards(new ArrayList<>()).build();
        Assert.assertEquals(hand.getCards().size(), 0);
    }

    @Test
    public void shouldSortRandomColor() {
        List<Color> colorsmock = Arrays.asList(Color.values());
        JeuRepository jeuRepositoryMock = mock(JeuRepository.class);
        JeuService jeuService = new JeuServiceImpl(jeuRepositoryMock);
        jeuService.sortRandomColor();
        Assert.assertThat(colorsmock, Matchers.hasItems(Color.COEUR));
        Assert.assertThat(colorsmock, Matchers.hasSize(4));
        Assert.assertThat(colorsmock, Matchers.containsInAnyOrder(Color.COEUR, Color.CARREAUX, Color.PIQUE, Color.TREFLE));
    }


    @Test
    public void shouldSortRandomValues() {
        List<Value> valuesmock = Arrays.asList(Value.values());
        JeuRepository jeuRepositoryMock = mock(JeuRepository.class);
        JeuService jeuService = new JeuServiceImpl(jeuRepositoryMock);
        jeuService.sortRandomValue();
        Assert.assertThat(valuesmock, Matchers.hasItems(Value.ROI));
        Assert.assertThat(valuesmock, Matchers.hasSize(13));
        Assert.assertThat(valuesmock, Matchers.containsInAnyOrder(Value.AS, Value.DEUX,
                Value.TROIS, Value.QUATRE, Value.CINQ, Value.SIX, Value.SEPT,
                Value.HUIT, Value.NEUF, Value.DIX, Value.ROI, Value.DAME, Value.VALET));
    }

    @Test
    public void shouldHaveTenCardInHand() {
        JeuRepository jeuRepositoryMock = mock(JeuRepository.class);
        JeuService jeuService = new JeuServiceImpl(jeuRepositoryMock);
        Hand hand = jeuService.tenCardInHand();
        Assert.assertEquals(hand.getCards().size(), 10);
    }

    @Test
    public void shouldSortHandWithColors() {
        Hand handmock = Hand.builder().cards(new ArrayList<>(Arrays.asList(new Card(Color.COEUR, Value.ROI),
                new Card(Color.COEUR, Value.CINQ), new Card(Color.PIQUE, Value.DAME), new Card(Color.CARREAUX, Value.DEUX),
                new Card(Color.PIQUE, Value.NEUF), new Card(Color.TREFLE, Value.TROIS), new Card(Color.COEUR, Value.HUIT),
                new Card(Color.CARREAUX, Value.DIX), new Card(Color.TREFLE, Value.AS), new Card(Color.PIQUE, Value.VALET)))).build();
        JeuRepository jeuRepositoryMock = mock(JeuRepository.class);
        JeuService jeuService = new JeuServiceImpl(jeuRepositoryMock);
        List<Color> colors = new ArrayList<>(Arrays.asList(Color.CARREAUX, Color.COEUR, Color.PIQUE, Color.TREFLE));
        List<Value> values = new ArrayList<>(Arrays.asList(Value.AS, Value.CINQ, Value.DIX, Value.HUIT,
                Value.SIX, Value.CINQ, Value.SEPT, Value.QUATRE, Value.DEUX, Value.TROIS, Value.NEUF,
                Value.DAME, Value.ROI, Value.VALET));
        Hand sortedHand = jeuService.sortHand(handmock.getCards(), colors, values);
        Assert.assertEquals(sortedHand.getCards().get(0).getColor(), Color.CARREAUX);
        Assert.assertEquals(sortedHand.getCards().get(0).getValue(), Value.DIX);
        Assert.assertEquals(sortedHand.getCards().get(2).getColor(), Color.COEUR);
        Assert.assertEquals(sortedHand.getCards().get(2).getValue(), Value.CINQ);
        Assert.assertEquals(sortedHand.getCards().get(5).getColor(), Color.PIQUE);
        Assert.assertEquals(sortedHand.getCards().get(5).getValue(), Value.NEUF);
        Assert.assertEquals(sortedHand.getCards().get(8).getColor(), Color.TREFLE);
        Assert.assertEquals(sortedHand.getCards().get(8).getValue(), Value.AS);
    }
}
