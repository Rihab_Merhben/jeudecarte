package com.example.demo.bdd;

import com.example.demo.DemoApplication;
import com.example.demo.model.Card;
import com.example.demo.model.Color;
import com.example.demo.model.Hand;
import com.example.demo.model.Value;
import com.example.demo.repository.JeuRepository;
import com.example.demo.service.JeuService;
import com.example.demo.service.JeuServiceImpl;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.hamcrest.Matchers;
import org.junit.Assert;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ContextConfiguration(classes = DemoApplication.class)
public class JeuBDDTest extends SpringBootBaseIntegrationTest{

    private JeuRepository jeuRepository;
    private JeuService jeuService =  new JeuServiceImpl(jeuRepository);
    private List<Card> cards;
    private List<Color> colors;
    private List<Value> values;
    private Hand hand;

    @Given("^As a hand$")
    public void as_a_hand(List<Card> cards) throws Exception{
        this.cards = cards;
    }

    @When("^I want to sort colors randomly")
    public void sortColors() throws Exception {
        colors = jeuService.sortRandomColor();
    }

    @When("^I want to sort values randomly")
    public void sortValues() throws Exception {
        values = jeuService.sortRandomValue();
    }

    @When("^I want to have 10 cards in my hand")
    public void have10Cards() throws Exception {
        hand = jeuService.tenCardInHand();
    }

    @When("^I want to sort my cards")
    public void sortHand() throws Exception {
        colors = jeuService.sortRandomColor();
        values = jeuService.sortRandomValue();
        hand = jeuService.tenCardInHand();
        hand = jeuService.sortHand(hand.getCards(), colors, values);
    }

    @Then("^The sorting colors is done$")
    public void validateSortingColors() throws Exception {
        assertEquals(colors.size(), 4);
        assertThat(colors, Matchers.hasItems(Color.COEUR));
        assertThat(colors, Matchers.containsInAnyOrder(Color.COEUR, Color.TREFLE, Color.PIQUE, Color.CARREAUX));
    }

    @Then("^The sorting values is done$")
    public void validateSortingValues() throws Exception {
        assertEquals(values.size(), 13);
        assertThat(values, Matchers.hasItems(Value.AS));
    }

    @Then("^Having 10 cards in my hand is done$")
    public void validateHaving10Cards() throws Exception {
        assertEquals(hand.getCards().size(), 10);
    }

    @Then("^The sorting hand is done$")
    public void validateSortingHand() throws Exception {
        assertEquals(hand.getCards().size(), 10);
        assertEquals(hand.getCards().get(0).getColor(), colors.get(0));
    }
}
