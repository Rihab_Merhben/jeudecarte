package com.example.demo.bdd;

import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features", plugin = { "pretty", "html:target/cucumber",
"json:target/cucumber.json" }, monochrome = true)
public class LaunchCucumberIntegrationTest {
	
}