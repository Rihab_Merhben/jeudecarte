package com.example.demo.bdd;

import com.example.demo.DemoApplication;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = DemoApplication.class)
public abstract class SpringBootBaseIntegrationTest {

}